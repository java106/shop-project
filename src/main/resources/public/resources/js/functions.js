/**
 * @param element
 * @returns {jQuery}
 * Creates list of uploaded files inf html form
 */
function getData(element) {
    const length = element.files.length;
    let output = '<ul class="list-group" style="margin: 10px 0 0 0;"><li class="list-group-item active">Lista załadowanych plików </li>';
    for (let i = 0; i < length; i++) {
        output += '<li class="list-group-item">' + element.files[i].name + '</li>';
    }
    output += '<ul>';
    return $('#img1').html(output);
}
