-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: shop_project
-- ------------------------------------------------------
-- Server version	5.7.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `active` tinyint(4) DEFAULT '0',
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (20,'michal@stopzone.pl','$2a$10$rpfgIRJTciMVsO0cwZf7NuAj9ZZZ/nDPhu9PWWvIIi/6qJRN15RzC','Michał Świątkowski',1,2),(21,'jan.kowalski@wp.pl','$2a$10$UVid6GA8LemaAhu6Bey.h.cLBv0qAsTrM779nzeET50Ct4ealcw/y','Jan Kowalska',0,1),(22,'test@gmail.com','$2a$10$IGcKB9DkXDmZEWsYefBv0OjfC17t.R.Tazdlu0//q9707ZlYxMJ4u','Michał testowy',1,1),(23,'john.doe@yohoo.com','$2a$10$7jd0yAsYQx8IPlVqErJZ1ePk7tFXbWh3p8uX1TmERROiMJpkFDjl.','John Doe',0,1),(24,'malysz@wp.eu','$2a$10$ypZ1bUc1NbDSg0PjBRWy/O3QuQQJvbgDQtPZz2qRuSaehLLPRvYoa','Adam Małysz',1,1),(25,'ala234@wp.pl','$2a$10$GlxBPPoTo3mkSC6jWvz1lOWbCTD2kc9nSEm6PirNhAsh3FoLpGMUi','Alicja Daniec',1,1),(26,'janek@gmail.com','$2a$10$rvAwUmj9UyxZlrkO.82dRupnT91AbecE7XLi/ICXKplJHXXN0QbVm','Jan Kochanowski',0,1),(27,'teresa34@wp.pl','$2a$10$gCcHkThrfbRVDbRXn0HG.upPeNBn6VrcE8cI1U6yxwYX3rL1aGOKK','Teresa Kaczmarek',0,1),(28,'maro23@gmail.com','$2a$10$51GuUwzidYdTy/coNhEmW.d.IE3dzgL5C8Mnir1dY4WHoh3cvMaA6','Mariusz Kocimski',0,1),(29,'ewa.kosmalska@wp.eu','$2a$10$9SVieYvMTxZCYYdhFihd7efKIOEOdEuWSO4aQES/3y1HuSMODC31.','Ewa Kosmalska',0,1),(30,'marek.konrad88@poczta.onet.pl','$2a$10$1qYfHN5mZdQXGl6RrokZYOQJMbz0B6eXzLWUPFZmgJqBtfvGPY4om','Marek Konrad',0,1),(31,'bozka1234@gmail.com','$2a$10$L.CfFxYg54ZriuEmOBGLqeqzbskUTG7CDYFKF3bxsX5fucnYR2Qr2','Bożena Kocerba',0,1),(32,'janek34@wp.pl','$2a$10$Ur6hdKA85Lrg06Sp4oOviOubve3twotR7yWRmvtYgU.EpdwrOZVwm','Jan Matejko',0,1),(33,'jarek56@wp.eu','$2a$10$.DIq8ELEBZ39tlZiKQBx..mCVUsjzmbRGFrQzFitb5AT3y2TiE6o6','Jarosław Piekarski',0,1),(34,'janusz453@wp.pl','$2a$10$AOZDlQrcnS1NL0JvGZwpQujLfMeZe0K7Qa.pFPqK6.3vCrkcDIGXW','Janusz Kołodziej',1,1),(35,'marek.n@wp.pl','$2a$10$SlDOhYyfWnGiYCe/o5rcs.Iw.4eKEKhMDW6ZGUA9cI5k7zdactg1W','Marek Nocny',1,1),(36,'janek@gmail.com','$2a$10$Oxg.t6yylO05cvYf5bLzy.tlx4P9N4ektNYP4JwU1MiojlEGFf2NS','Jan Kossak',1,1),(37,'joanna23.zaduma@wp.pl','$2a$10$uDUTod577di2JYosdE5fkO0mitMQCiUBnCyFycRSArdZ8kQbf0Jv.','Joanna Zaduma',1,1),(38,'pawel.test@wp.pl','$2a$10$3thmZjIdXNoPrE7dQmeweO8lwwQFCwM8EjAVO37GyDSlzbeMI2rvK','Janusz Paweł',1,1),(39,'marek.n@wp.pl','$2a$10$EPIPBTCzHWqAJJcQjY2WXe20Vg6gx6d655jtrq13.mt7N1HQRJarq','Marek Nocny',1,1),(40,'janek@gmail.com','$2a$10$6fZ.HRUkkMKmB9ZvIgaNO.wEHLSXtj8YXp98Y7vSrilsEKmzngE8e','Jan Kossak',1,1),(41,'joanna23.zaduma@wp.pl','$2a$10$GSKHnpo7i91KCAP.YWqxh.ePBy2qqX4BtrJWua0LTp4kRa8b.WCn2','Joanna Zaduma',1,1),(42,'janusz453@wp.pl','$2a$10$QrDDbP9BNKlj39BKAMkcpODlnVrFvTNUsoqVJFbR8DSL.xRVEu5y.','Janusz Kołodziej',1,1),(43,'marek.n@wp.pl','$2a$10$SP32A1ufWRmtHbr8nAPnwO1K9lO8W6SQHspt/xqwUfCiNkY14wOH2','Marek Nocny',1,1),(44,'janek@gmail.com','$2a$10$daJalp6bPyWLSbc9OyUGOOs/fwrjmmXkhn9STF4mTB7/q.6FX0ypa','Jan Kossak',1,1),(45,'joanna23.zaduma@wp.pl','$2a$10$FlSOIebnxnyXncKvdtfVteFDXkxkIUGyjP9dBXsr0CxP85PV9Jq6C','Joanna Zaduma',1,1),(46,'marek@wp.it','$2a$10$deO5LB62ya6GcOFmUNsQG.nXJVsTUF7tNHdSV60ZWh5DQhTdIQq9u','Marek Łatek',0,1),(47,'mm@wp.pl','$2a$10$W3.MyqWsrJnXC7fPDmbb2eph.wKQFqe3rBIBj2mEKgcfD4f1RB4E2','Michał Marecki',0,1),(48,'nowymichal@wp.pl','$2a$10$nA5EewiaMp45Qi1FiSCcKeB30H53bgD/91GyF.gXFa0BlDgF.cRYe','Michał Michał',0,1),(49,'michaltest@wp.pl','$2a$10$pF8EcnBzf7CNlXz4dMpUK.YvtLjE/4HQMC3SfEVZgStzP13jeSvXG','Michal',0,1),(50,'ttt@wp.pl','$2a$10$X5uZVdnjhNGsX4a/KwiH2uuMaXuxw1AnTM4SlRk1niWJz75Ydf1wi','michał',0,1),(51,'toja@wp.pl','$2a$10$ej6gY0Xnxq4mwNYn0Qo/mO99mnAMU3CXlV2agkbPInHXsBkwcfrju','Michał Świątkowski',0,1),(52,'michal@wp.pl','$2a$10$QUIJ1GVpbtBTCc83PkmyNufqbC6LQXpjBOh60zTebDSPtAPyzCDuW','Michał Testowy User2',1,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-07 10:26:02
