-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: shop_project
-- ------------------------------------------------------
-- Server version	5.7.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(250) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_images_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=272 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_images`
--

LOCK TABLES `product_images` WRITE;
/*!40000 ALTER TABLE `product_images` DISABLE KEYS */;
INSERT INTO `product_images` VALUES (45,'office.jpg',3),(55,'pamiec.jpg',2),(56,'pamiec_1.jpg',2),(58,'tel1.jpg',4),(59,'tel2.jpg',4),(60,'tel3.jpg',4),(61,'dp2.webp',5),(62,'gp1.webp',5),(63,'gp2.webp',5),(64,'gp3.webp',5),(65,'dr1.webp',6),(66,'dr2.webp',6),(67,'dr3.webp',6),(68,'dr4.webp',6),(69,'ssd0.webp',7),(70,'ssd1.webp',7),(71,'ssd2.webp',7),(73,'lap1.webp',8),(74,'lap2.webp',8),(75,'lap4.webp',8),(76,'lalp3.webp',8),(77,'tab1.webp',9),(78,'tab2.webp',9),(79,'tab3.webp',9),(80,'tab4.webp',9),(81,'surface1.webp',10),(82,'surface2.webp',10),(83,'smart1.webp',11),(84,'smart2.webp',11),(85,'smart3.webp',11),(86,'dron1.webp',12),(87,'dron2.webp',12),(88,'dron3.webp',12),(89,'dron4.webp',12),(90,'kartapamieci.webp',13),(91,'monitor.webp',14),(92,'monitor2.webp',14),(93,'monitor3.webp',14),(94,'monitor4.webp',14),(95,'usb1.webp',15),(96,'usb2.webp',15),(97,'usb3.webp',15),(98,'usb4.webp',15),(99,'komp1.webp',16),(100,'komp2.webp',16),(101,'komp3.webp',16),(102,'kabel1.webp',17),(103,'kabel2.webp',17),(104,'obudowa1.webp',18),(105,'obudowa2.webp',18),(106,'obudowa3.webp',18),(107,'obudowa4.webp',18),(108,'obudowa5.webp',18),(109,'jbl1.webp',19),(110,'jbl2.webp',19),(111,'jbl3.webp',19),(112,'jbl4.webp',19),(113,'tp1.webp',20),(114,'tp2.webp',20),(115,'tp3.webp',20),(116,'tp4.webp',20),(117,'mik1.webp',21),(118,'mik2.webp',21),(119,'mik3.webp',21),(120,'amd.webp',22),(121,'mg1.webp',23),(122,'mg2.webp',23),(123,'mg3.webp',23),(124,'mg4.webp',23),(125,'mg5.webp',23),(126,'gopro1.webp',24),(127,'gopro2.webp',24),(128,'aparat1.webp',25),(129,'aparat2.webp',25),(130,'aparat3.webp',25),(131,'glosnik1.webp',26),(132,'glosnik2.webp',26),(133,'kino1.webp',27),(134,'eset.webp',28),(135,'listwa1.webp',29),(136,'listwa2.webp',29),(137,'gta1.png',1),(138,'sluchawki_1.jpg',30),(139,'sluchawki_2.jpg',30),(140,'sluchawki_3.jpg',30),(141,'mysza_1.jpg',31),(142,'mysza_2.jpg',31),(143,'mysza_3.jpg',31),(144,'mysza_4.jpg',31),(145,'klawiatura_3.jpg',32),(146,'klawiatura_4.jpg',32),(147,'klawiatura_1.jpg',32),(148,'klawiatura_2.jpg',32),(149,'ps4_1.jpg',33),(150,'ps4_2.jpg',33),(151,'ps4_3.jpg',33),(152,'xbox_1.jpg',34),(153,'xbox_2.jpg',34),(154,'xbox_3.jpg',34),(155,'lapek-1.jpg',35),(156,'lapek-2.jpg',35),(157,'lapek-3.jpg',35),(158,'lapek-4.jpg',35),(159,'dell-1.jpg',36),(160,'dell-2.jpg',36),(161,'dell-3.jpg',36),(162,'dell-4.jpg',36),(163,'dell-5.jpg',36),(164,'dell-6.jpg',36),(165,'dell-7.jpg',36),(166,'kruge-1.jpg',37),(167,'kruge-2.jpg',37),(168,'kruge-3.jpg',37),(169,'kruge-4.jpg',37),(170,'tablet-1.jpg',38),(171,'tablet-2.jpg',38),(172,'tablet-3.jpg',38),(173,'tablet-4.jpg',38),(174,'huawei-1.jpg',39),(175,'huawei-2.jpg',39),(176,'huawei-3.jpg',39),(177,'huawei-4.jpg',39),(178,'huawei-5.jpg',39),(179,'i5-1.jpg',40),(180,'i5-2.jpg',40),(181,'i5-3.jpg',40),(182,'i5-4.jpg',40),(183,'dyskzew-1.jpg',41),(184,'dyskzew-2.jpg',41),(185,'dyskzew-3.jpg',41),(186,'dyskzew-4.jpg',41),(187,'ram-1.jpeg',42),(188,'ram-2.jpeg',42),(189,'ram-3.jpeg',42),(190,'ram-4.jpeg',42),(191,'grafika-1.jpg',43),(192,'grafika-2.jpg',43),(193,'grafika-3.jpg',43),(194,'grafika-4.jpg',43),(195,'grafika-5.jpg',43),(196,'adata-1.jpg',44),(197,'adata-2.jpg',44),(198,'adata-3.jpg',44),(199,'adata-4.jpg',44),(200,'adata-5.jpg',44),(201,'iphine-2.jpg',45),(202,'iphone-1.jpg',45),(203,'xiaomi-1.jpg',46),(204,'xiaomi-2.jpg',46),(205,'xiaomi-3.jpg',46),(206,'xiaomi-4.jpg',46),(207,'iphone_4.jpg',47),(208,'iphone_1.jpg',47),(209,'iphone_2.jpg',47),(210,'iphone_3.jpg',47),(211,'iphone_5.jpg',47),(212,'navi-1.jpg',48),(213,'navi-2.jpg',48),(214,'navi-3.jpg',48),(215,'navi-4.jpg',48),(216,'navi_1.jpg',49),(217,'kamera-1.png',50),(218,'kamera-2.png',50),(219,'kamera-3.png',50),(220,'kamera-4.png',50),(221,'kamera-5.png',50),(222,'kamera_1.jpg',51),(223,'kamera_2.jpg',51),(224,'kam_sam-1.jpg',52),(225,'kam_sam-2.jpg',52),(226,'kam_sam-3.jpg',52),(227,'kam_sam-4.jpg',52),(228,'kam_sam-5.jpg',52),(229,'clamp-1.jpg',53),(230,'clamp-2.jpg',53),(231,'clamp-3.jpg',53),(232,'clamp-4.jpg',53),(233,'gopro_gopro.jpg',54),(234,'odtw-1.jpg',55),(235,'odtw-2.jpg',55),(236,'odtw-3.jpg',55),(237,'amplituner-1.jpg',56),(238,'amplituner-2.jpg',56),(239,'amplituner-3.jpg',56),(240,'amplituner-4.jpg',56),(241,'glosniki-1.jpg',57),(242,'glosniki-2.jpg',57),(243,'rewizor.jpg',58),(244,'reset2.jpg',59),(245,'insert.png',60),(246,'lamax-1.jpg',61),(247,'lamax-2.jpg',61),(248,'lamax-3.jpg',61),(249,'lamax-4.jpg',61),(250,'ladowarka-1.jpg',62),(251,'ladowarka-2.jpg',62),(252,'ladowarka-3.jpg',62),(253,'ladowarka-4.jpg',62),(254,'ladowarka-5.jpg',62),(255,'wacom-1.jpeg',63),(256,'wacom-2.jpeg',63),(257,'wacom-3.jpeg',63),(258,'wacom-4.jpeg',63),(259,'amazon-1.jpg',64),(260,'amazon-2.jpg',64),(261,'amazon-3.jpg',64),(262,'amazon-4.jpg',64),(263,'canon-1.jpeg',65),(264,'canon-2.jpeg',65),(265,'tplink-0.jpg',66),(266,'tplink-1.jpg',66),(267,'tplink-2.jpg',66),(268,'qnap-1.png',67),(269,'qnap-2.png',67),(270,'qnap-3.png',67),(271,'qnap-4.png',67);
/*!40000 ALTER TABLE `product_images` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-07 10:26:02
