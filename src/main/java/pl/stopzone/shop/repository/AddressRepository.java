package pl.stopzone.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.stopzone.shop.model.Address;
import pl.stopzone.shop.model.User;

import java.util.List;

public interface AddressRepository extends JpaRepository<Address, Long>  {

    List<Address> findAllByUserId(User userId);
    @Query("select s from Address s where s.id = ?1")
    Address findAddressById(Integer id);
    
}
