package pl.stopzone.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.stopzone.shop.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

}
