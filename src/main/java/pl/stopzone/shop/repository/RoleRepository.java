package pl.stopzone.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.stopzone.shop.model.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByRoleName(String role);

    @Query("select s from Role s where s.id = ?1")
    Role findRoleById(Integer roleId);
}
