package pl.stopzone.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.stopzone.shop.model.Image;
import pl.stopzone.shop.model.Product;

import java.util.List;

public interface ImageRepository extends JpaRepository<Image, Long> {
    List<Image> findAllByProductId(Product product);
    void deleteById(Long imageId);
}
