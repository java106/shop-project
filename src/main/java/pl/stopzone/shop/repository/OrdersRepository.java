package pl.stopzone.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.stopzone.shop.model.Order;
import pl.stopzone.shop.model.User;

import java.util.List;

public interface OrdersRepository extends JpaRepository<Order, Long> {

    List<Order> findAllByUser(User user);
}
