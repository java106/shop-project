package pl.stopzone.shop.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.stopzone.shop.model.Category;
import pl.stopzone.shop.model.Product;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findAllByOrderByCategoryIdAsc();
    List<Product> findAllByCategoryId(Category category);
    List<Product> findAll();

    @Query("select s from Product s where s.name like %?1%")
    Page<Product> findByProductName(String productName, PageRequest pageable);
}
