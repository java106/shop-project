package pl.stopzone.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.stopzone.shop.model.Order;
import pl.stopzone.shop.model.OrderProduct;

import java.util.List;

public interface OrderProductRepository extends JpaRepository<OrderProduct, Long> {

    List<OrderProduct> findAllByOrder(Order order);

}
