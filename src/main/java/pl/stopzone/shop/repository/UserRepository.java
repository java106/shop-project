package pl.stopzone.shop.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import pl.stopzone.shop.model.Role;
import pl.stopzone.shop.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);

    @Query("select s from User s where s.email like %?1%")
    Page<User> findByEmail(String email, PageRequest pageable);
    User findById(Integer id);

    @Modifying
    @Query("update User u set u.password = ?1, u.name = ?2, u.active = ?3, u.roleId = ?4 where u.id = ?5")
    void updateAllUserData(String password, String name, Integer active, Role roleId, Long userId);
}
