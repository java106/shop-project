package pl.stopzone.shop.mapper;

import lombok.NoArgsConstructor;
import pl.stopzone.shop.dto.UserDto;
import pl.stopzone.shop.model.User;

@NoArgsConstructor
public class UserMapper {

    public static User mapFromDto(UserDto user) {

        User u = new User();
        u.setId(Math.toIntExact(user.getId()));
        u.setActive(user.getActive());
        u.setName(user.getName());
        u.setPassword(user.getPassword());
        u.setRoleId(user.getRoleId());
        u.setEmail(user.getEmail());

        return u;

    }

    public static UserDto mapToDto(User user) {

        UserDto userDto = new UserDto();
        userDto.setId((long) user.getId());
        userDto.setName(user.getName());
        userDto.setActive(user.getActive());
        userDto.setEmail(user.getEmail());
        userDto.setRoleId(user.getRoleId());

        return userDto;

    }

}
