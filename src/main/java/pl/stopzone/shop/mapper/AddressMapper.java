package pl.stopzone.shop.mapper;

import lombok.NoArgsConstructor;
import pl.stopzone.shop.dto.AddressDto;
import pl.stopzone.shop.model.Address;

@NoArgsConstructor
public class AddressMapper {

    public static Address addressToDao(AddressDto addressDto) {

        Address address = new Address();
        address.setId(Math.toIntExact(addressDto.getId()));
        address.setStreet(addressDto.getStreet());
        address.setZipcode(addressDto.getZipcode());
        address.setCity(addressDto.getCity());
        address.setUserId(addressDto.getUserId());

        return address;

    }

    public static AddressDto mapToDto(Address address) {

        AddressDto addressDto = new AddressDto();

        addressDto.setId(address.getId());
        addressDto.setCity(address.getCity());
        addressDto.setStreet(address.getStreet());
        addressDto.setZipcode(address.getZipcode());

        return addressDto;

    }

}
