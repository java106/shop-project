package pl.stopzone.shop.mapper;

import pl.stopzone.shop.dto.ProductDto;
import pl.stopzone.shop.model.Category;
import pl.stopzone.shop.model.Product;

import java.math.BigDecimal;

public class ProductMapper {

    public static Product mapDtoToDao(ProductDto productDto) {

        Product p = new Product();

        Category category = new Category();
        category.setId(productDto.getCategoryId());
        category.setName(productDto.getCategoryName());

        p.setId(productDto.getProductId());
        p.setCategoryId(category);
        p.setDescription(productDto.getProductDescription());
        p.setName(productDto.getProductName());
        p.setNumberProducts(productDto.getNumberProducts());
        p.setPrice(new BigDecimal(productDto.getProductPrice()));

        return p;

    }

}
