package pl.stopzone.shop.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@NoArgsConstructor
@Data
@ToString
public class ProductDto {

    private Long productId;
    private String productName;
    private String productDescription;
    private String productPrice;
    private Integer numberProducts;
    private Long categoryId;
    private String categoryName;
    private List<ImageDto> imagesList;

    public ProductDto(String productName, String productDescription, String productPrice, Integer numberProducts, Long categoryId) {
        this.productName = productName;
        this.productDescription = productDescription;
        this.productPrice = productPrice;
        this.numberProducts = numberProducts;
        this.categoryId = categoryId;
    }

    public ProductDto(Long productId, String productName, String productDescription, String productPrice,
                      Integer numberProducts, Long categoryId, String categoryName, List<ImageDto> imagesList) {
        this.productId = productId;
        this.productName = productName;
        this.productDescription = productDescription;
        this.productPrice = productPrice;
        this.numberProducts = numberProducts;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.imagesList = imagesList;
    }

}
