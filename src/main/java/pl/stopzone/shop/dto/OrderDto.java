package pl.stopzone.shop.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {

    private Long id;
    private UserDto user;
    private AddressDto address;
    private BigDecimal brutto;
    private BigDecimal netto;
    private String date;
    private List<OrderProductDto> productsList;

}
