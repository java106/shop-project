package pl.stopzone.shop.dto;

import lombok.*;
import pl.stopzone.shop.model.User;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class AddressDto {

    private long id;
    private String city;
    private String street;
    private String zipcode;
    private User userId;

}
