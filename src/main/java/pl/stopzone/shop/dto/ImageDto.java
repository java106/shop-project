package pl.stopzone.shop.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.stopzone.shop.model.Product;

@Data
@NoArgsConstructor
public class ImageDto {

    public ImageDto(Long id, String path, Product productId) {
        this.id = id;
        this.path = path;
        this.productId = productId;
    }

    public ImageDto(Long id, String path) {
        this.id = id;
        this.path = path;
    }

    private Long id;
    private String path;
    private Product productId;

}
