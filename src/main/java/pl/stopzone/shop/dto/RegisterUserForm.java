package pl.stopzone.shop.dto;

import lombok.*;
import pl.stopzone.shop.model.Role;

@Data
@ToString
public class RegisterUserForm {

    private String name;
    private String email;
    private String password;
    private String rpassword;
    private Role roleid;
    private int active;

}
