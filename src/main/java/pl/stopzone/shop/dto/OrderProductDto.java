package pl.stopzone.shop.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.stopzone.shop.model.Product;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderProductDto {

    private Long id;
    private Product product;
    private String productPrice;
    private Integer productNumber;

}
