package pl.stopzone.shop.dto;

import lombok.*;
import pl.stopzone.shop.model.Role;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class UserDto {

    public UserDto(long id, String email, String password, String name, Integer active, Role roleId) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.name = name;
        this.active = active;
        this.roleId = roleId;
    }

    private Long id;
    private String email;
    private String password;
    private String name;
    private Integer active;
    private Role roleId;

    private Integer roleIdLongType;
    private String rpassword;

    public String getRoleName() {
        return roleId.getRoleName();
    }

    private List<AddressDto> addressList;

}