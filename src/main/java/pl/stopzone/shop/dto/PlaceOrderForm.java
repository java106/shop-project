package pl.stopzone.shop.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PlaceOrderForm {

    private Long addressDto;

}
