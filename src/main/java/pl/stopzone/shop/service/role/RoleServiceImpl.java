package pl.stopzone.shop.service.role;

import org.springframework.stereotype.Service;
import pl.stopzone.shop.dto.RoleDto;
import pl.stopzone.shop.model.Role;
import pl.stopzone.shop.repository.RoleRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<RoleDto> findAll() {

        List<Role> roles = roleRepository.findAll();
        List<RoleDto> out = new ArrayList<>();

        for (Role role : roles) {
            out.add(new RoleDto(role.getId(), role.getRoleName()));
        }

        return out;

    }

}
