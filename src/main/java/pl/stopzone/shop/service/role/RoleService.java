package pl.stopzone.shop.service.role;

import pl.stopzone.shop.dto.RoleDto;

import java.util.List;

public interface RoleService {
    List<RoleDto> findAll();
}
