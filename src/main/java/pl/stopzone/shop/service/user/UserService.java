package pl.stopzone.shop.service.user;

import org.springframework.data.domain.Page;
import pl.stopzone.shop.dto.RegisterUserForm;
import pl.stopzone.shop.dto.UserDto;
import pl.stopzone.shop.model.User;

import java.util.Optional;

public interface UserService {
    User saveUser(RegisterUserForm form);
    void updateUser(UserDto userDto);
    User findByEmail(String email);
    UserDto findDataByUserEmail(String email);
    Page<User> getUsers(Optional<String> email, Optional<Integer> page, Optional<String> sortBy, Integer nrUserPerPage);
    UserDto findById(Integer id);
}
