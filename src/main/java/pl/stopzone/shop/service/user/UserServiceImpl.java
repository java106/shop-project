package pl.stopzone.shop.service.user;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.stopzone.shop.dto.RegisterUserForm;
import pl.stopzone.shop.dto.UserDto;
import pl.stopzone.shop.model.Role;
import pl.stopzone.shop.model.User;
import pl.stopzone.shop.repository.RoleRepository;
import pl.stopzone.shop.repository.UserRepository;
import pl.stopzone.shop.service.address.AddressService;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User saveUser(RegisterUserForm form) {

        User user = new User();
        user.setPassword(passwordEncoder.encode(form.getPassword()));
        user.setActive(0);
        user.setName(form.getName());
        user.setEmail(form.getEmail());
        user.setRoleId(roleRepository.findByRoleName("ROLE_USER"));

        return userRepository.save(user);

    }

    @Override
    public void updateUser(UserDto userDto) {

        User user = userRepository.findById(userDto.getId().intValue());

        if (userDto.getRoleId() == null) {

            if (StringUtils.isNoneBlank(userDto.getPassword())) {
                user.setPassword(passwordEncoder.encode(userDto.getPassword()));
            }

            user.setName(userDto.getName());

        } else {

            int active = (userDto.getActive() == null) ? 0 : 1;
            Role role = roleRepository.findRoleById(userDto.getRoleIdLongType());

            if (StringUtils.isNoneBlank(userDto.getPassword())) {
                user.setPassword(passwordEncoder.encode(userDto.getPassword()));
            }

            user.setActive(active);
            user.setName(userDto.getName());
            user.setRoleId(role);

        }

        userRepository.save(user);

    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Page<User> getUsers(Optional<String> email, Optional<Integer> page, Optional<String> sortBy, Integer numberUsersPerPage) {

//        List<UserDto> usersList = new ArrayList<>();
//        for (User user : users) {
//            usersList.add(new UserDto(user.getId(), user.getEmail(), user.getPassword(), user.getName(), user.getActive(), user.getRoleId()));
//        }

        return userRepository.findByEmail(email.orElse(""),
                PageRequest.of(page.orElse(0), numberUsersPerPage, Sort.Direction.ASC, sortBy.orElse("id")));

    }

    @Override
    public UserDto findById(Integer id) {
        User user =  userRepository.findById(id);
        return new UserDto(user.getId(), user.getEmail(), user.getPassword(), user.getName(), user.getActive(), user.getRoleId());
    }

    @Override
    public UserDto findDataByUserEmail(String email) {
        User byEmail = userRepository.findByEmail(email);



        UserDto userDto = new UserDto();
        userDto.setId((long) byEmail.getId());
        userDto.setEmail(byEmail.getEmail());
        userDto.setName(byEmail.getName());
        userDto.setActive(byEmail.getActive());
        userDto.setRoleId(byEmail.getRoleId());
        userDto.setAddressList(null);

        return userDto;
    }

}
