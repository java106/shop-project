package pl.stopzone.shop.service.order;

import pl.stopzone.shop.component.ShoppingCart;
import pl.stopzone.shop.dto.PlaceOrderForm;
import pl.stopzone.shop.dto.ProductDto;
import pl.stopzone.shop.dto.UserDto;

import java.math.BigDecimal;
import java.util.List;

public interface PlaceOrderService {
    Long saveOrder(PlaceOrderForm address, UserDto user, BigDecimal orderValue, ShoppingCart cart);
}
