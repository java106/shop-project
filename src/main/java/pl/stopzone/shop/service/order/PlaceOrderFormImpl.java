package pl.stopzone.shop.service.order;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.stopzone.shop.component.ShoppingCart;
import pl.stopzone.shop.dto.PlaceOrderForm;
import pl.stopzone.shop.dto.ProductDto;
import pl.stopzone.shop.dto.UserDto;
import pl.stopzone.shop.mapper.ProductMapper;
import pl.stopzone.shop.mapper.UserMapper;
import pl.stopzone.shop.model.Address;
import pl.stopzone.shop.model.Order;
import pl.stopzone.shop.model.OrderProduct;
import pl.stopzone.shop.model.User;
import pl.stopzone.shop.repository.AddressRepository;
import pl.stopzone.shop.repository.OrderProductRepository;
import pl.stopzone.shop.repository.OrdersRepository;
import pl.stopzone.shop.repository.ProductRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class PlaceOrderFormImpl implements PlaceOrderService {

    private ProductRepository productRepository;
    private OrdersRepository ordersRepository;
    private OrderProductRepository orderProductRepository;
    private AddressRepository addressRepository;


    public PlaceOrderFormImpl(ProductRepository productRepository, OrdersRepository ordersRepository, OrderProductRepository orderProductRepository, AddressRepository addressRepository) {
        this.productRepository = productRepository;
        this.ordersRepository = ordersRepository;
        this.orderProductRepository = orderProductRepository;
        this.addressRepository = addressRepository;
    }

    @Override
    @Transactional
    public Long saveOrder(PlaceOrderForm form, UserDto user, BigDecimal orderValue, ShoppingCart cart) {

        Order order = new Order();
        User userDao = UserMapper.mapFromDto(user);
        Address addressDao = addressRepository.findAddressById(Math.toIntExact(form.getAddressDto()));

        order.setUser(userDao);
        order.setAddress(addressDao);
        order.setBrutto(orderValue);
        order.setNetto(orderValue.divide(new BigDecimal("1.23"), 2, RoundingMode.HALF_UP));
        Order savedOrder = ordersRepository.save(order);

        for (ProductDto product : cart.getProducts()) {
            OrderProduct op = new OrderProduct();
            op.setOrder(savedOrder);
            op.setNumberPorducts(1);
            op.setProductInOrder(ProductMapper.mapDtoToDao(product));
            op.setProductPrice(new BigDecimal(product.getProductPrice()));
            orderProductRepository.save(op);
        }

        return order.getId();

    }

}
