package pl.stopzone.shop.service.order;

import pl.stopzone.shop.dto.OrderDto;
import pl.stopzone.shop.model.User;

import java.util.List;

public interface OrderService {
    List<OrderDto> getAllOrdersByUser(User user);
}
