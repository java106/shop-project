package pl.stopzone.shop.service.order;

import org.springframework.stereotype.Service;
import pl.stopzone.shop.dto.OrderDto;
import pl.stopzone.shop.dto.OrderProductDto;
import pl.stopzone.shop.mapper.AddressMapper;
import pl.stopzone.shop.mapper.UserMapper;
import pl.stopzone.shop.model.Order;
import pl.stopzone.shop.model.OrderProduct;
import pl.stopzone.shop.model.User;
import pl.stopzone.shop.repository.OrderProductRepository;
import pl.stopzone.shop.repository.OrdersRepository;
import pl.stopzone.shop.service.product.ProductService;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private OrdersRepository orderRepository;
    private OrderProductRepository orderProductRepository;
    private ProductService productService;

    public OrderServiceImpl(OrdersRepository orderRepository, OrderProductRepository orderProductRepository, ProductService productService) {
        this.orderRepository = orderRepository;
        this.orderProductRepository = orderProductRepository;
        this.productService = productService;
    }

    @Override
    public List<OrderDto> getAllOrdersByUser(User user) {

        List<Order> ordersByUser = orderRepository.findAllByUser(user);

        List<OrderDto> output = new ArrayList<>();

        for (Order order : ordersByUser) {

            List<OrderProductDto> orderProductDto = new ArrayList<>();

            List<OrderProduct> allByOrder = orderProductRepository.findAllByOrder(order);
            for (OrderProduct orderProduct : allByOrder) {
                orderProductDto.add(new OrderProductDto(
                        orderProduct. getId(),
                        orderProduct.getProductInOrder(),
                        orderProduct.getProductPrice().toString(),
                        orderProduct.getNumberPorducts()
                ));
            }

            output.add(new OrderDto(
                    order.getId(),
                    UserMapper.mapToDto(user),
                    AddressMapper.mapToDto(order.getAddress()),
                    order.getBrutto(),
                    order.getNetto(),
                    order.getDate(),
                    orderProductDto
            ));

        }

        return output;

    }

}
