package pl.stopzone.shop.service.category;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.stopzone.shop.dto.CategoryDto;
import pl.stopzone.shop.model.Category;
import pl.stopzone.shop.repository.CategoryRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<CategoryDto> findAllCategories() {

        List<Category> categories = categoryRepository.findAll();
        List<CategoryDto> out = new ArrayList<>();

        for (Category category : categories) {
            out.add(new CategoryDto(category.getId(), category.getName()));
        }

        return out;

    }

    @Override
    public CategoryDto findById(Long id) {

        Optional<Category> category = categoryRepository.findById(id);
        CategoryDto categoryDto = new CategoryDto();
        category.ifPresent(c -> {
            categoryDto.setId(c.getId());
            categoryDto.setName(c.getName());
        });

        return categoryDto;

    }

}
