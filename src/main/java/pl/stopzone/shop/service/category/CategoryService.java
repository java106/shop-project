package pl.stopzone.shop.service.category;

import pl.stopzone.shop.dto.CategoryDto;
import pl.stopzone.shop.model.Category;

import java.util.List;

public interface CategoryService {
    List<CategoryDto> findAllCategories();
    CategoryDto findById(Long id);
}
