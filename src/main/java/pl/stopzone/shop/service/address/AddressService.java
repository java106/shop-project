package pl.stopzone.shop.service.address;

import pl.stopzone.shop.dto.AddressDto;

import java.util.List;

public interface AddressService {
    void saveAddress(AddressDto addressDto, Integer userId);
    List<AddressDto> findByUserId(Integer id);
    List<AddressDto> findByUserEmail(String email);
    void deleteAddress(Integer id);
    AddressDto findById(Long id);

}
