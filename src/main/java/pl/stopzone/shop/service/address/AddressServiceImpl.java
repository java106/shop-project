package pl.stopzone.shop.service.address;

import org.springframework.stereotype.Service;
import pl.stopzone.shop.dto.AddressDto;
import pl.stopzone.shop.model.Address;
import pl.stopzone.shop.model.User;
import pl.stopzone.shop.repository.AddressRepository;
import pl.stopzone.shop.repository.UserRepository;
import pl.stopzone.shop.service.user.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService {

    private AddressRepository addressRepository;
    private UserRepository userRepository;

    public AddressServiceImpl(AddressRepository addressRepository, UserService userService, UserRepository userRepository) {
        this.addressRepository = addressRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void saveAddress(AddressDto addressDto, Integer userId) {

        Address address = new Address();
        address.setCity(addressDto.getCity());
        address.setStreet(addressDto.getStreet());
        address.setZipcode(addressDto.getZipcode());
        address.setUserId(userRepository.findById(userId));
        addressRepository.save(address);

    }

    @Override
    public List<AddressDto> findByUserId(Integer id) {

        List<Address> address = addressRepository.findAllByUserId(userRepository.findById(id));
        List<AddressDto> output = new ArrayList<>();

        for (Address ad : address) {
            output.add(new AddressDto(ad.getId(), ad.getCity(), ad.getStreet(), ad.getZipcode(), ad.getUserId()));
        }

        return output;

    }

    @Override
    public void deleteAddress(Integer id) {

        Address addressById = addressRepository.findAddressById(id);
        addressRepository.delete(addressById);

    }

    @Override
    public AddressDto findById(Long id) {
        Optional<Address> address = addressRepository.findById(id);
        AddressDto out = new AddressDto();
        address.ifPresent(a -> {
            out.setId(a.getId());
            out.setCity(a.getCity());
            out.setStreet(a.getStreet());
            out.setZipcode(a.getZipcode());
            out.setUserId(null);
        });

        return out;
    }

    @Override
    public List<AddressDto> findByUserEmail(String email) {

        User user = userRepository.findByEmail(email);

        List<AddressDto> addressDtos = new ArrayList<>();
        List<Address> address = new ArrayList<>();

        if (user != null) {
            address = addressRepository.findAllByUserId(user);
        }

        for (Address ad : address) {
            addressDtos.add(new AddressDto(ad.getId(), ad.getCity(), ad.getStreet(), ad.getZipcode(), ad.getUserId()));
        }

        return addressDtos;

    }

}
