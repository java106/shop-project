package pl.stopzone.shop.service.product;

import org.springframework.data.domain.Page;
import pl.stopzone.shop.dto.ProductDto;
import pl.stopzone.shop.model.Product;
import pl.stopzone.shop.model.User;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    Product saveProduct(ProductDto addProductFormDto);
    List<Product> findAll();
    Product findById(Long id);
    /*
        value 1 : get sorted data by category
        value 2 : get unsorted data
     */
    List<ProductDto> getAllProducts(Integer value);
    ProductDto findProductWithImages(Long productId);
    List<ProductDto> findAllProductsByCategory(Long id);

    Page<ProductDto> getAllProductsPageable(Optional<String> productName, Optional<Integer> page, Optional<String> sortBy, Optional<String> order, Integer nrProductsPerPage);
    ProductDto getProductById(Long id);
}
