package pl.stopzone.shop.service.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.stopzone.shop.dto.ImageDto;
import pl.stopzone.shop.dto.ProductDto;
import pl.stopzone.shop.model.Category;
import pl.stopzone.shop.model.Image;
import pl.stopzone.shop.model.Product;
import pl.stopzone.shop.repository.CategoryRepository;
import pl.stopzone.shop.repository.ImageRepository;
import pl.stopzone.shop.repository.ProductRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;
    private CategoryRepository categoryRepository;
    private ImageRepository imageRepository;

    public ProductServiceImpl(ProductRepository productRepository, CategoryRepository categoryRepository, ImageRepository imageRepository) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
        this.imageRepository = imageRepository;
    }

    @Override
    public Product saveProduct(ProductDto form) {

        Optional<Category> category = categoryRepository.findById(form.getCategoryId());
        Product newProduct = new Product();

        category.ifPresent(cat -> {
            newProduct.setName(form.getProductName());
            newProduct.setDescription(form.getProductDescription());
            newProduct.setPrice(new BigDecimal(form.getProductPrice().replace(",", ".")));
            newProduct.setNumberProducts(form.getNumberProducts());
            newProduct.setCategoryId(cat);
        });

        productRepository.save(newProduct);

        return newProduct;

    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Product findById(Long id) {
        Optional<Product> product = productRepository.findById(id);
        Product findProduct = new Product();

        product.ifPresent(product1 -> {
            findProduct.setId(product1.getId());
            findProduct.setCategoryId(product1.getCategoryId());
            findProduct.setNumberProducts(product1.getNumberProducts());
            findProduct.setPrice(product1.getPrice());
            findProduct.setDescription(product1.getDescription());
            findProduct.setName(product1.getName());
        });

        return findProduct;

    }

    @Override
    public List<ProductDto> getAllProducts(Integer value) {

        List<ProductDto> output = new ArrayList<>();
        List<Product> allProducts = new ArrayList<Product>();

        if (value.equals(1)) {
            allProducts = productRepository.findAllByOrderByCategoryIdAsc();
        } else {
            allProducts = productRepository.findAll();
        }

        for (Product product : allProducts) {
            List<Image> allImagesByProductId = imageRepository.findAllByProductId(product);
            List<ImageDto> imagesByProduct = new ArrayList<>();
            for (Image image : allImagesByProductId) {
                imagesByProduct.add(new ImageDto(
                        image.getId(), image.getPath(), image.getProductId()
                ));
            }

            output.add(new ProductDto(
                    product.getId(), product.getName(), product.getDescription(), product.getPrice().toString(),
                    product.getNumberProducts(), product.getCategoryId().getId(), product.getCategoryId().getName(), imagesByProduct
            ));
        }

        return output;

    }

    @Override
    public ProductDto findProductWithImages(Long productId) {

        Optional<Product> product = productRepository.findById(productId);
        ProductDto output = new ProductDto();

        product.ifPresent(p -> {

            List<Image> allByProductId = imageRepository.findAllByProductId(p);
            List<ImageDto> images = new ArrayList<>();
            for (Image image : allByProductId) {
                images.add(new ImageDto(
                        image.getId(), image.getPath()
                ));
            }

            Category category = categoryRepository.getOne(p.getCategoryId().getId());

            output.setProductId(p.getId());
            output.setProductName(p.getName());
            output.setProductDescription(p.getDescription());
            output.setProductPrice(p.getPrice().toString());
            output.setNumberProducts(p.getNumberProducts());
            output.setImagesList(images);
            output.setCategoryName(category.getName());
            output.setCategoryId(category.getId());

        });

        return output;

    }

    @Override
    public List<ProductDto> findAllProductsByCategory(Long id) {

        Optional<Category> category = categoryRepository.findById(id);
        List<ProductDto> products = new ArrayList<>();

        category.ifPresent(c -> {

            List<Product> allProducts = productRepository.findAllByCategoryId(c);
            for (Product product : allProducts) {

                List<Image> allImagesByProduct = imageRepository.findAllByProductId(product);
                List<ImageDto> images = new ArrayList<>();
                for (Image image : allImagesByProduct) {
                    images.add(new ImageDto(
                            image.getId(), image.getPath()
                    ));
                }

                products.add(new ProductDto(
                        product.getId(), product.getName(), product.getDescription(), product.getPrice().toString(),
                        product.getNumberProducts(), c.getId(), c.getName(), images
                ));
            }

        });

        return products;

    }

    @Override
    public Page<ProductDto> getAllProductsPageable(Optional<String> productName, Optional<Integer> page,
                                                   Optional<String> sortBy, Optional<String> order, Integer nrProductsPerPage) {

        Page<Product> productsListDao = productRepository.findByProductName(productName.orElse(""),
                PageRequest.of(page.orElse(0), nrProductsPerPage, Sort.Direction.fromString(order.orElse("ASC")), sortBy.orElse("id")));

        return productsListDao.map(new Function<Product, ProductDto>() {

            @Override
            public ProductDto apply(Product entity) {

                List<Image> allImagesByProductId = imageRepository.findAllByProductId(entity);
                List<ImageDto> imagesByProduct = new ArrayList<>();
                for (Image image : allImagesByProductId) {
                    imagesByProduct.add(new ImageDto(
                            image.getId(), image.getPath(), image.getProductId()
                    ));
                }

                ProductDto dto = new ProductDto();

                dto.setProductId(entity.getId());
                dto.setProductName(entity.getName());
                dto.setProductDescription(entity.getDescription());
                dto.setProductPrice(entity.getPrice().toString());
                dto.setNumberProducts(entity.getNumberProducts());
                dto.setCategoryId(entity.getCategoryId().getId());
                dto.setCategoryName(entity.getCategoryId().getName());
                dto.setImagesList(imagesByProduct);

                return dto;
            }

        });

    }

    @Override
    public ProductDto getProductById(Long id) {

        Product one = productRepository.getOne(id);
        List<Image> images = imageRepository.findAllByProductId(one);
        List<ImageDto> imagesDto = images.stream().map(i -> new ImageDto(i.getId(), i.getPath())).collect(Collectors.toList());

        return new ProductDto(one.getId(), one.getName(), one.getDescription(), one.getPrice().toString(),
                one.getNumberProducts(), one.getCategoryId().getId(), one.getCategoryId().getName(), imagesDto);

    }

}
