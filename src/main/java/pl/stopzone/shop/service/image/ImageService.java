package pl.stopzone.shop.service.image;

import pl.stopzone.shop.dto.ImageDto;
import pl.stopzone.shop.model.Product;

import java.util.List;

public interface ImageService {
    List<ImageDto> findAllImagesByProductId(Product product);
    void addImage(String imagesName, Product product);
    void deleteImage(Long imageId);
    ImageDto findOne(Long imageId);
    List<ImageDto> findAllImages();
}
