package pl.stopzone.shop.service.image;

import org.springframework.stereotype.Service;
import pl.stopzone.shop.dto.ImageDto;
import pl.stopzone.shop.model.Image;
import pl.stopzone.shop.model.Product;
import pl.stopzone.shop.repository.ImageRepository;
import pl.stopzone.shop.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ImageServiceImpl implements ImageService {

    private ImageRepository imageRepository;

    public ImageServiceImpl(ImageRepository imageRepository, ProductRepository productRepository) {
        this.imageRepository = imageRepository;
    }

    @Override
    public List<ImageDto> findAllImagesByProductId(Product product) {

        List<Image> allByProductId = imageRepository.findAllByProductId(product);
        List<ImageDto> images = new ArrayList<>();

        for (Image image : allByProductId) {
            images.add(new ImageDto(image.getId(), image.getPath(), product));
        }

        return images;

    }

    @Override
    public void addImage(String imagesName, Product product) {

        Image image = new Image();
        image.setPath(imagesName);
        image.setProductId(product);

        imageRepository.save(image);

    }

    @Override
    public void deleteImage(Long imageId) {
        imageRepository.deleteById(imageId);
    }

    @Override
    public ImageDto findOne(Long imageId) {

        Optional<Image> image = imageRepository.findById(imageId);
        ImageDto out = new ImageDto();
        image.ifPresent(image1 -> {
            out.setId(image1.getId());
            out.setPath(image1.getPath());
            out.setProductId(image1.getProductId());
        });

        return out;

    }

    @Override
    public List<ImageDto> findAllImages() {

        List<Image> allImages = imageRepository.findAll();
        List<ImageDto> images = new ArrayList<>();

        for (Image image : allImages) {
            images.add(new ImageDto(
                    image.getId(), image.getPath(), image.getProductId()
            ));
        }

        return images;

    }

}
