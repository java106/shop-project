package pl.stopzone.shop.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    private String name;
    private String description;
    private BigDecimal price;

    @Column(name = "number_of_products")
    private Integer numberProducts;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "category_id")
    private Category categoryId;

//    @ManyToMany(mappedBy = "productsList")
//    private List<OrderProduct> orders = new ArrayList<>();

}
