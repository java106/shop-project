package pl.stopzone.shop.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private int id;

    @Column(name = "email", nullable = false, updatable = false)
    private String email;
    private String password;
    private String name;
    private int active;

    public User(String email, String password, String name, int active) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.active = active;
    }

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "role_id")
    private Role roleId;

}

