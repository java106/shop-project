package pl.stopzone.shop.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    private BigDecimal brutto;
    private BigDecimal netto;

    @Column(name = "date_order", updatable = false, insertable = false)
    private String date;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "address_id")
    private Address address;

//    @Column(name = "date_order")
//    private String datetTime;

    @OneToMany
    private List<OrderProduct> orderProduct;

}
