package pl.stopzone.shop.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@ToString
@Table(name = "addresses")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Integer id;

    private String city;
    private String street;
    private String zipcode;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "user_id")
    private User userId;

}
