package pl.stopzone.shop.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.stopzone.shop.dto.PlaceOrderForm;

public class CreateOrderFormValidation implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return PlaceOrderForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        PlaceOrderForm placeOrderForm = (PlaceOrderForm) o;

        if(placeOrderForm.getAddressDto() == null) {
            errors.rejectValue("addressDto", "user.filed.empty");
        }

    }

}
