package pl.stopzone.shop.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.stopzone.shop.dto.UserDto;

public class UpdateUserValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return UserDto.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        UserDto form = (UserDto) o;

        if(StringUtils.isBlank(form.getName())) {
            errors.rejectValue("name", "user.filed.empty");
        }

        if(!StringUtils.isBlank(form.getPassword())) {
            if (!form.getPassword().equals(form.getRpassword())) {
                errors.rejectValue("password", "user.filed.password.repeat");
            }
        }

    }

}
