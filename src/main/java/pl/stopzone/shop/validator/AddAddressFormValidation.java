package pl.stopzone.shop.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.stopzone.shop.dto.AddressDto;

@Component
public class AddAddressFormValidation implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return AddressDto.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        AddressDto address = (AddressDto) o;

        if(StringUtils.isBlank(address.getCity())) {
            errors.rejectValue("city", "user.filed.empty");
        }

        if(StringUtils.isBlank(address.getZipcode())) {
            errors.rejectValue("zipcode", "user.filed.empty");
        }

        if(StringUtils.isBlank(address.getStreet())) {
            errors.rejectValue("street", "user.filed.empty");
        }

    }
}
