package pl.stopzone.shop.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.stopzone.shop.dto.ProductDto;

public class AddProductFormValidation implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return ProductDto.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

            ProductDto addProductFormDto = (ProductDto) o;

        if(StringUtils.isBlank(addProductFormDto.getProductName())) {
            errors.rejectValue("productName", "user.filed.empty");
        }

    }
}
