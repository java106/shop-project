package pl.stopzone.shop.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.stopzone.shop.dto.RegisterUserForm;

@Component
public class CreateUserFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return RegisterUserForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        RegisterUserForm form = (RegisterUserForm) o;
        String email_regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";

        if(StringUtils.isBlank(form.getName())) {
            errors.rejectValue("name", "user.filed.empty");
        }

        if(StringUtils.isBlank(form.getPassword())) {
            errors.rejectValue("password", "user.filed.empty");
        } else {
            if (!form.getPassword().equals(form.getRpassword())) {
                errors.rejectValue("password", "user.filed.password.repeat");
            }
        }

        if (StringUtils.isBlank((form.getEmail()))) {
            errors.rejectValue("email", "user.filed.empty");
        } else {
            if (!form.getEmail().matches(email_regex)) {
                errors.rejectValue("email", "user.invalid.email");
            }
        }

    }
}
