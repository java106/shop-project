package pl.stopzone.shop.controller.admin.user;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.stopzone.shop.controller.admin.core.AdminBreadcrumbs;
import pl.stopzone.shop.controller.admin.core.AdminController;
import pl.stopzone.shop.dto.AddressDto;
import pl.stopzone.shop.dto.RegisterUserForm;
import pl.stopzone.shop.dto.UserDto;
import pl.stopzone.shop.model.User;
import pl.stopzone.shop.service.address.AddressService;
import pl.stopzone.shop.service.role.RoleService;
import pl.stopzone.shop.service.user.UserService;
import pl.stopzone.shop.validator.AddAddressFormValidation;
import pl.stopzone.shop.validator.CreateUserFormValidator;
import pl.stopzone.shop.validator.UpdateUserValidator;

import java.util.Optional;

@Controller
public class AdminUserController extends AdminController {

    private UserService userService;
    private AddressService addressService;
    private RoleService roleService;

    @InitBinder("addressDto")
    void iniBinderUser(WebDataBinder binder) { binder.setValidator(new AddAddressFormValidation()); }

    @InitBinder("userUpdate")
    void iniBinderUpdateUser(WebDataBinder binder) { binder.setValidator(new UpdateUserValidator()); }

    @InitBinder("newUserForm")
    void iniBinderNewUser(WebDataBinder binder) { binder.setValidator(new CreateUserFormValidator()); }

    public AdminUserController(UserService userService, AddressService addressService, RoleService roleService) {
        super();
        this.userService = userService;
        this.addressService = addressService;
        this.roleService = roleService;
    }

    @GetMapping("/admin/users")
    public ModelAndView showAllUsers(@RequestParam Optional<String> email,
                                 @RequestParam Optional<Integer> page,
                                 @RequestParam Optional<String> sortBy) {

        ModelAndView mnv = new ModelAndView("adm/index");

        Page<User> usersPage =  userService.getUsers(email, page, sortBy, 10);

        AdminBreadcrumbs br = new AdminBreadcrumbs();
        br.add("Użytkownicy", "/admin/users/");
        br.add("Lista użytkowników", "");

        mnv.addObject("loggedUsername", getUserName());
        mnv.addObject("content", "usersList");
        mnv.addObject("contentpath", "user/usersList");
        mnv.addObject("title", "Users List - Admin Panel");
        mnv.addObject("usersList", usersPage);
        mnv.addObject("totalPages", usersPage.getTotalPages());
        mnv.addObject("page", page.orElse(0));
        mnv.addObject("email", email.orElse(null));
        mnv.addObject("countUsers", usersPage.getTotalElements());
        mnv.addObject("breadcrumbs", br.get());

        return mnv;
    }

    @GetMapping("/admin/users/show")
    public String showRedirect() {
        return "redirect:/admin/users";
    }

    @GetMapping("/admin/users/edit")
    public String editRedirect() {
        return "redirect:/admin/users";
    }

    @GetMapping("/admin/users/show/{userId}")
    public ModelAndView showOneUser(@PathVariable(required = true, value = "userId") Integer id, RedirectAttributes redirectAttributes) {

        ModelAndView mnv = new ModelAndView("adm/index");

        AdminBreadcrumbs br = new AdminBreadcrumbs();
        br.add("Użytkownicy", "/admin/users/");
        br.add("Informacje o użytkowniku", "/admin/users/show/" + id);
        br.add(getUserName(), "");

        mnv.addObject("loggedUsername", getUserName());
        mnv.addObject("content", "userInfo");
        mnv.addObject("contentpath", "user/userInfo");
        mnv.addObject("title", "Users Details - Admin Panel");
        mnv.addObject("user", userService.findById(id));
        mnv.addObject("address", addressService.findByUserId(id));
        mnv.addObject("breadcrumbs", br.get());
        mnv.addObject("addressDto", new AddressDto());

        return mnv;
    }

    @PostMapping("/admin/users/show/{id}")
    public String createNewAddress(@ModelAttribute("addressDto") @Validated AddressDto addressDto, BindingResult bindingResult,
                                   @PathVariable("id") Integer userid, RedirectAttributes redirectAttributes) {

        if (!bindingResult.hasErrors()) {
            addressService.saveAddress(addressDto, userid);
            redirectAttributes.addFlashAttribute("successMessage", "Dodano nowy adres.");
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", "Formularz zawiera błędy.");
        }
        return "redirect:/admin/users/show/" + userid;

    }

    @GetMapping("/admin/users/edit/{userId}")
    public ModelAndView editUserData(@PathVariable(required = true, value = "userId") Integer id, RedirectAttributes redirectAttributes) {

        ModelAndView mnv = new ModelAndView("adm/index");

        AdminBreadcrumbs br = new AdminBreadcrumbs();
        br.add("Użytkownicy", "/admin/users/");
        br.add("Edycja użytkownika", "/admin/users/edit/" + id);
        br.add(getUserName(), "");

        mnv.addObject("loggedUsername", getUserName());
        mnv.addObject("content", "userEdit");
        mnv.addObject("contentpath", "user/userEdit");
        mnv.addObject("title", "Edit User - Admin Panel");
        mnv.addObject("breadcrumbs", br.get());
        mnv.addObject("roles",roleService.findAll());
        UserDto user = userService.findById(id);
        mnv.addObject("userUpdate", user);

        return mnv;
    }

    @PostMapping("/admin/users/edit/{id}")
    public String updateUserData(@ModelAttribute("userUpdate") @Validated UserDto userUpdate, BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes, @PathVariable("id") Integer userid) {

        if (!bindingResult.hasErrors()) {
            userService.updateUser(userUpdate);
            redirectAttributes.addFlashAttribute("successMessage", "Poprawnie zaktualizowano użytkownika.");
            return "redirect:/admin/users/show/" + userid;
        }

        redirectAttributes.addFlashAttribute("errorMessage", "Formularz zawiera błędy.");
        return "redirect:/admin/users/edit/" + userid;

    }

    @GetMapping("/admin/users/{user}/delete/address/{address}")
    public String deleteAddressFromUser(@PathVariable(required = true, value = "user") Integer userId,
                                        @PathVariable(required = true, value = "address") Integer addressId, RedirectAttributes redirectAttributes) {

        addressService.deleteAddress(addressId);
        redirectAttributes.addFlashAttribute("successMessage", "Poprawnie usunięto adres.");
        return "redirect:/admin/users/show/" + userId ;

    }

    @GetMapping("/admin/users/add")
    public String addUserDataForm(Model model) {

        AdminBreadcrumbs br = new AdminBreadcrumbs();
        br.add("Użytkownicy", "/admin/users/");
        br.add("Nowy użytkownik", "");

        model.addAttribute("loggedUsername", getUserName());
        model.addAttribute("content", "userAdd");
        model.addAttribute("contentpath", "user/userAdd");
        model.addAttribute("title", "Add User - Admin Panel");
        model.addAttribute("breadcrumbs", br.get());
        model.addAttribute("newUserForm", new RegisterUserForm());

        return "adm/index";

    }

    @PostMapping("/admin/users/add")
    public String addUserData(@ModelAttribute("newUserForm") @Validated RegisterUserForm newUserForm, BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes, Model model) {

        AdminBreadcrumbs br = new AdminBreadcrumbs();
        br.add("Użytkownicy", "/admin/users/");
        br.add("Nowy użytkownik", "");

        model.addAttribute("loggedUsername", getUserName());
        model.addAttribute("content", "userAdd");
        model.addAttribute("contentpath", "user/userAdd");
        model.addAttribute("title", "Add User - Admin Panel");
        model.addAttribute("breadcrumbs", br.get());

        if (!bindingResult.hasErrors()) {
            User userExists = userService.findByEmail(newUserForm.getEmail());

            if (userExists != null) {
                bindingResult.rejectValue("email", "error.user","Podany adres e-mail istnieje w systemie.");
            } else {
                User savedUser = userService.saveUser(newUserForm);
                redirectAttributes.addFlashAttribute("successMessage", "Dodano nowego użytkownika.");
                return "redirect:/admin/users/show/" + savedUser.getId() ;
            }
        }

        return "adm/index";

    }

}
