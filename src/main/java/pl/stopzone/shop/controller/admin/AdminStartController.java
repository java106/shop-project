package pl.stopzone.shop.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.stopzone.shop.controller.admin.core.AdminController;
import pl.stopzone.shop.controller.admin.core.AdminBreadcrumbs;

@Controller
@RequestMapping("/admin")
public class AdminStartController extends AdminController {

    public AdminStartController() {
        super();
    }

    @RequestMapping
    public String index(Model model) {

        AdminBreadcrumbs br = new AdminBreadcrumbs();
        br.add("Dashboard", "");

        model.addAttribute("loggedUsername", getUserName());
        model.addAttribute("content", "dashboard");
        model.addAttribute("contentpath", "dashboard/dashboard");
        model.addAttribute("title", "Dashboard - Admin Panel");
        model.addAttribute("breadcrumbs", br.get());
        return "adm/index";
    }

}