package pl.stopzone.shop.controller.admin.product;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.stopzone.shop.controller.admin.core.AdminBreadcrumbs;
import pl.stopzone.shop.controller.admin.core.AdminController;
import pl.stopzone.shop.dto.ImageDto;
import pl.stopzone.shop.model.Product;
import pl.stopzone.shop.service.image.ImageService;
import pl.stopzone.shop.service.product.ProductService;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
public class AdminImageController extends AdminController {

    @Value("${products.images.uploadDir}")
    private String uploadDir;

    private ProductService productService;
    private ImageService imageService;

    public AdminImageController(ProductService productService, ImageService imageService) {
        this.productService = productService;
        this.imageService = imageService;
    }

    @GetMapping("/admin/images/add/{productId}")
    public String addImages(@PathVariable(required = true, value = "productId") String id, RedirectAttributes redirectAttributes, Model model) {

        Product product = productService.findById(Long.valueOf(id));

        AdminBreadcrumbs br = new AdminBreadcrumbs();
        br.add("Produkty", "/admin/products/");
        br.add("Dodawanie obrazka do produktu", "/admin/products/show/" + id);
        br.add(product.getName(), "");

        model.addAttribute("loggedUsername", getUserName());
        model.addAttribute("content", "imageAdd");
        model.addAttribute("contentpath", "product/imageAdd");
        model.addAttribute("title", "Add Images - Admin Panel");
        model.addAttribute("breadcrumbs", br.get());
        model.addAttribute("product", product);

        return "adm/index";
    }

    @GetMapping("/admin/images/delete/{productId}/{imageId}")
    public String imageRemove(@PathVariable(required = true, value = "productId") Long productId,
                              @PathVariable(required = true, value = "imageId") Long imageId, RedirectAttributes redirectAttributes) {

        ImageDto image = imageService.findOne(imageId);
        if (image != null) {
            File file = new File(uploadDir + image.getPath());
            if (file.delete()) {
                imageService.deleteImage(imageId);
                redirectAttributes.addFlashAttribute("successMessage", "Plik został usunięty.");
                return "redirect:/admin/products/show/" + productId;
            }
        }

        redirectAttributes.addFlashAttribute("errorMessage", "Plik nie może zostać usunięty.");
        return "redirect:/admin/products/show/" + productId;
    }

    @PostMapping("/admin/images/add/{productId}")
    public String imageUpload(@RequestParam("img") MultipartFile[] file, @PathVariable(required = true, value = "productId") Long id,
                                   RedirectAttributes redirectAttributes) {

        Product product = productService.findById(id);

        if(product != null) {

            for (MultipartFile multipartFile : file) {

                if (multipartFile.isEmpty()) {
                    redirectAttributes.addFlashAttribute("errorMessage", "Wybierz plik do przesłania na serwer.");
                    return "redirect:/admin/images/add/" + id;
                }

                try {

                    byte[] bytes = multipartFile.getBytes();
                    Path path = Paths.get(uploadDir + multipartFile.getOriginalFilename());
                    Files.write(path, bytes);

                    imageService.addImage(multipartFile.getOriginalFilename(), product);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }

        return "redirect:/admin/products/show/" + id;

    }


}
