package pl.stopzone.shop.controller.admin.product;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.stopzone.shop.controller.admin.core.AdminBreadcrumbs;
import pl.stopzone.shop.controller.admin.core.AdminController;
import pl.stopzone.shop.dto.ImageDto;
import pl.stopzone.shop.dto.ProductDto;
import pl.stopzone.shop.model.Product;
import pl.stopzone.shop.service.category.CategoryService;
import pl.stopzone.shop.service.image.ImageService;
import pl.stopzone.shop.service.product.ProductService;
import pl.stopzone.shop.validator.AddProductFormValidation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Controller
public class AdminProductController extends AdminController {

    @Value("${products.images.uploadDir}")
    private String uploadDir;

    private CategoryService categoryService;
    private ProductService productService;
    private ImageService imageService;

    public AdminProductController(CategoryService categoryService, ProductService productService, ImageService imageService) {
        this.categoryService = categoryService;
        this.productService = productService;
        this.imageService = imageService;
    }

    @InitBinder("newProductForm")
    void iniBinderUser(WebDataBinder binder) { binder.setValidator(new AddProductFormValidation()); }

    @GetMapping("/admin/products")
    public ModelAndView showAllProducts() {

        ModelAndView mnv = new ModelAndView("adm/index");

        List<ProductDto> allProducts = productService.getAllProducts(1);

        AdminBreadcrumbs br = new AdminBreadcrumbs();
        br.add("Produkty", "/admin/products/");
        br.add("Lista produktów", "");

        mnv.addObject("loggedUsername", getUserName());
        mnv.addObject("content", "usersList");
        mnv.addObject("contentpath", "product/productsList");
        mnv.addObject("title", "Products List - Admin Panel");
        mnv.addObject("productsList", allProducts);
        mnv.addObject("breadcrumbs", br.get());

        return mnv;
    }

    @GetMapping("/admin/products/add")
    public String addProductData(Model model) {

        AdminBreadcrumbs br = new AdminBreadcrumbs();
        br.add("Produkty", "/admin/products/");
        br.add("Nowy produkt", "");

        model.addAttribute("loggedUsername", getUserName());
        model.addAttribute("content", "productAdd");
        model.addAttribute("contentpath", "product/productAdd");
        model.addAttribute("title", "Add Product - Admin Panel");
        model.addAttribute("breadcrumbs", br.get());
        model.addAttribute("newProductForm", new ProductDto());
        model.addAttribute("categories", categoryService.findAllCategories());

        return "adm/index";

    }

    @GetMapping("/admin/products/show/{id}")
    public String showOneProduct(@PathVariable(required = true, value = "id") String id, RedirectAttributes redirectAttributes, Model model) {

        Product product = productService.findById(Long.valueOf(id));
        List<ImageDto> allImagesByProductId = imageService.findAllImagesByProductId(product);

        AdminBreadcrumbs br = new AdminBreadcrumbs();
        br.add("Produkty", "/admin/products/");
        br.add("Informacje o produkcie", "/admin/products/show/" + id);
        br.add(product.getName(), "");

        model.addAttribute("loggedUsername", getUserName());
        model.addAttribute("content", "productInfo");
        model.addAttribute("contentpath", "product/productInfo");
        model.addAttribute("title", "Add Product - Admin Panel");
        model.addAttribute("breadcrumbs", br.get());
        model.addAttribute("product", product);
        model.addAttribute("images", allImagesByProductId);

        return "adm/index";
    }

    @PostMapping("/admin/products/add")
    public String addProductData(@RequestParam("img") MultipartFile[] file, @ModelAttribute("newProductForm") @Validated ProductDto newProductForm,
                                 BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        AdminBreadcrumbs br = new AdminBreadcrumbs();
        br.add("Produkty", "/admin/products/");
        br.add("Nowy produkt", "");

        model.addAttribute("loggedUsername", getUserName());
        model.addAttribute("content", "productAdd");
        model.addAttribute("contentpath", "product/productAdd");
        model.addAttribute("title", "Add Product - Admin Panel");
        model.addAttribute("breadcrumbs", br.get());
        model.addAttribute("newProductForm", newProductForm);
        model.addAttribute("categories", categoryService.findAllCategories());

        if (!bindingResult.hasErrors()) {
            Product product = productService.saveProduct(newProductForm);

            for (MultipartFile multipartFile : file) {

                try {

                    byte[] bytes = multipartFile.getBytes();
                    Path path = Paths.get(uploadDir + multipartFile.getOriginalFilename());
                    Files.write(path, bytes);

                    imageService.addImage(multipartFile.getOriginalFilename(), product);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            redirectAttributes.addFlashAttribute("successMessage", "Nowy produkt został dodany poprawnie.");
            return "redirect:/admin/products/show/" + product.getId();
        }

        return "adm/index";

    }

}
