package pl.stopzone.shop.controller.admin.core;

import java.util.LinkedHashMap;
import java.util.Map;

public class AdminBreadcrumbs {

    private Map<String, String> breadcrumbs = new LinkedHashMap<>();

    public AdminBreadcrumbs() {
        this.breadcrumbs.put("Admin", "/admin/");
    }

    public void add(String s1, String s2) {
        breadcrumbs.put(s1, s2);
    }

    public Map<String, String> get() {
        return breadcrumbs;
    }

}