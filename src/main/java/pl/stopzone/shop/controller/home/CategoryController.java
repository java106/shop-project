package pl.stopzone.shop.controller.home;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.stopzone.shop.component.ShoppingCart;
import pl.stopzone.shop.dto.CategoryDto;
import pl.stopzone.shop.dto.ProductDto;
import pl.stopzone.shop.service.category.CategoryService;
import pl.stopzone.shop.service.product.ProductService;

import java.util.List;

@Controller
public class CategoryController {

    private ProductService productService;
    private CategoryService categoryService;
    private ShoppingCart shoppingCart;

    public CategoryController(ProductService productService, CategoryService categoryService, ShoppingCart shoppingCart) {
        this.productService = productService;
        this.categoryService = categoryService;
        this.shoppingCart = shoppingCart;
    }

    @GetMapping("/category/{id}")
    public String getOneCategory(@PathVariable(required = true, value = "id") Long id, Model model) {

        List<ProductDto> products = productService.findAllProductsByCategory(id);
        CategoryDto category = categoryService.findById(id);

        model.addAttribute("content", "categoryPublic");
        model.addAttribute("title", "Kategoria: " + category.getName());
        model.addAttribute("categories", categoryService.findAllCategories());
        model.addAttribute("categoryName", category.getName());
        model.addAttribute("categoryId", category.getId());
        model.addAttribute("products", products);
        model.addAttribute("size", shoppingCart.countCartProducts());

        return "home/index";
    }

}
