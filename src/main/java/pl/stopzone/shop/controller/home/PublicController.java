package pl.stopzone.shop.controller.home;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.stopzone.shop.component.ShoppingCart;
import pl.stopzone.shop.dto.ProductDto;
import pl.stopzone.shop.service.category.CategoryService;
import pl.stopzone.shop.service.product.ProductService;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class PublicController {

    private CategoryService categoryService;
    private ProductService productService;
    private ShoppingCart shoppingCart;

    public PublicController(CategoryService categoryService, ProductService productService, ShoppingCart shoppingCart) {
        this.categoryService = categoryService;
        this.productService = productService;
        this.shoppingCart = shoppingCart;
    }

    @RequestMapping
    public String index(@RequestParam Optional<Integer> page,
                        @RequestParam Optional<String> sortBy, Optional<String> order, Model model) {

        Page<ProductDto> allProductsPageable = productService.getAllProductsPageable(Optional.empty(), page, sortBy, order, 16);

//        allProductsPageable.forEach(p -> System.out.println(p.getProductPrice()));

        Map<String, String> sort = new LinkedHashMap<>() {{
            put("Cena: od najniższej", "/?sortBy=price&order=ASC");
            put("Cena: od najwyższej", "/?sortBy=price&order=DESC");
            put("Dostępność: od najniższej", "/?sortBy=numberProducts&order=ASC");
            put("Dostępność: od najwyższej", "/?sortBy=numberProducts&order=DESC");

        }};

        model.addAttribute("content", "indexPublic");
        model.addAttribute("title", "Strona główna");
        model.addAttribute("totalPages", allProductsPageable.getTotalPages());
        model.addAttribute("page", page.orElse(0));
        model.addAttribute("countProducts", allProductsPageable.getTotalElements());
        model.addAttribute("categories", categoryService.findAllCategories());
        model.addAttribute("products", allProductsPageable);
        model.addAttribute("sortMap", sort);
        model.addAttribute("_sortBy", sortBy.orElse(null));
        model.addAttribute("_order", order.orElse(null));
        model.addAttribute("size", shoppingCart.countCartProducts());


        return "home/index";
    }

}
