package pl.stopzone.shop.controller.home;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.stopzone.shop.component.ShoppingCart;
import pl.stopzone.shop.dto.ProductDto;
import pl.stopzone.shop.service.category.CategoryService;
import pl.stopzone.shop.service.product.ProductService;

@Controller
public class ProductController {

    private ProductService productService;
    private CategoryService categoryService;
    private ShoppingCart shoppingCart;

    public ProductController(ProductService productService, CategoryService categoryService, ShoppingCart shoppingCart) {
        this.productService = productService;
        this.categoryService = categoryService;
        this.shoppingCart = shoppingCart;
    }

    @GetMapping("/product/{id}")
    public String getOneProductPublic(@PathVariable(required = true, value = "id") Long id, Model model) {

        ProductDto product = productService.findProductWithImages(id);

        model.addAttribute("content", "productPublic");
        model.addAttribute("title", product.getProductName());
        model.addAttribute("categories", categoryService.findAllCategories());
        model.addAttribute("product", product);
        model.addAttribute("size", shoppingCart.countCartProducts());

        return "home/index";
    }

}
