package pl.stopzone.shop.controller.home;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.stopzone.shop.component.ShoppingCart;
import pl.stopzone.shop.service.category.CategoryService;

@Controller
@RequestMapping("/cart")
public class CartController {

    private final ShoppingCart shoppingCart;
    private CategoryService categoryService;

    public CartController(ShoppingCart shoppingCart, CategoryService categoryService) {
        this.shoppingCart = shoppingCart;
        this.categoryService = categoryService;
    }

    @RequestMapping("/all")
    public String getAllProductsFromCart(Model model){

        model.addAttribute("content", "cartPublic");
        model.addAttribute("title", "Koszyk");
        model.addAttribute("categories", categoryService.findAllCategories());
        model.addAttribute("cart", shoppingCart.getProducts());
        model.addAttribute("sum", shoppingCart.cartSumValue());
        model.addAttribute("size", shoppingCart.countCartProducts());

        return "home/index";

    }

    @RequestMapping("/add/{productId}")
    public String addProducId(@PathVariable Long productId) {
        shoppingCart.addProductToCart(productId);
        return "redirect:/cart/all";
    }

    @RequestMapping("/del/{productId}")
    public String delProducId(@PathVariable Long productId) {
        shoppingCart.removeFromCart(productId);
        return "redirect:/cart/all";
    }

}