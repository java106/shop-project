package pl.stopzone.shop.controller.home;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.stopzone.shop.component.ShoppingCart;
import pl.stopzone.shop.dto.ProductDto;
import pl.stopzone.shop.service.category.CategoryService;
import pl.stopzone.shop.service.product.ProductService;

import java.util.Optional;

@Controller
@RequestMapping("/search")
public class SearchController {

    private ProductService productService;
    private CategoryService categoryService;
    private ShoppingCart shoppingCart;

    public SearchController(ProductService productService, CategoryService categoryService, ShoppingCart shoppingCart) {
        this.productService = productService;
        this.categoryService = categoryService;
        this.shoppingCart = shoppingCart;
    }

    @GetMapping
    public String index(@RequestParam Optional<String> name, @RequestParam Optional<Integer> page, Model model) {

        Page<ProductDto> searchedProducts = productService.getAllProductsPageable(name, page, Optional.empty(), Optional.empty(), 10);

        model.addAttribute("content", "searchPublic");
        model.addAttribute("title", "Wyszukiwarka");
        model.addAttribute("totalPages", searchedProducts.getTotalPages());
        model.addAttribute("page", page.orElse(0));
        model.addAttribute("countProducts", searchedProducts.getTotalElements());
        model.addAttribute("categories", categoryService.findAllCategories());
        model.addAttribute("products", searchedProducts);
        model.addAttribute("searchName", name.orElse(null));
        model.addAttribute("size", shoppingCart.countCartProducts());

        return "home/index";
    }

}
