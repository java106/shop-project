package pl.stopzone.shop.controller.home;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.stopzone.shop.component.ShoppingCart;
import pl.stopzone.shop.dto.PlaceOrderForm;
import pl.stopzone.shop.dto.UserDto;
import pl.stopzone.shop.service.address.AddressService;
import pl.stopzone.shop.service.category.CategoryService;
import pl.stopzone.shop.service.order.PlaceOrderService;
import pl.stopzone.shop.service.user.UserService;
import pl.stopzone.shop.validator.CreateOrderFormValidation;

@Controller
@RequestMapping("/place-order")
public class PlaceOrderController {

    private final ShoppingCart shoppingCart;
    private CategoryService categoryService;
    private UserService userService;
    private AddressService addressService;
    private PlaceOrderService placeOrderService;

    public PlaceOrderController(ShoppingCart shoppingCart, CategoryService categoryService, UserService userService, AddressService addressService, PlaceOrderService placeOrderService) {
        this.shoppingCart = shoppingCart;
        this.categoryService = categoryService;
        this.userService = userService;
        this.addressService = addressService;
        this.placeOrderService = placeOrderService;
    }

    @InitBinder("placeOrderForm")
    void iniBinderNewUser(WebDataBinder binder) { binder.setValidator(new CreateOrderFormValidation()); }

    @GetMapping
    public String placeOrder(PlaceOrderForm placeOrderForm, Model model) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();

        UserDto user = userService.findDataByUserEmail(userName);
        user.setAddressList(addressService.findByUserEmail(userName));

        model.addAttribute("content", "placeOrderPublic");
        model.addAttribute("title", "Składanie zamówienia");
        model.addAttribute("categories", categoryService.findAllCategories());
        model.addAttribute("cart", shoppingCart.getProducts());
        model.addAttribute("sum", shoppingCart.cartSumValue());
        model.addAttribute("user", user);
        model.addAttribute("size", shoppingCart.countCartProducts());
        model.addAttribute("placeOrderForm", placeOrderForm);

        return "home/index";

    }

    @PostMapping
    public String placeOrder(@ModelAttribute("placeOrderForm") @Validated PlaceOrderForm placeOrderForm,
                             BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        model.addAttribute("content", "placeOrderPublic");
        model.addAttribute("title", "Składanie zamówienia");
        model.addAttribute("categories", categoryService.findAllCategories());
        model.addAttribute("cart", shoppingCart.getProducts());
        model.addAttribute("sum", shoppingCart.cartSumValue());
        model.addAttribute("size", shoppingCart.countCartProducts());
        model.addAttribute("form", placeOrderForm);



        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth.isAuthenticated()) {

            if (shoppingCart.countCartProducts() > 0) {

                String userName = auth.getName();
                UserDto user = userService.findDataByUserEmail(userName);
                user.setAddressList(addressService.findByUserEmail(userName));
                model.addAttribute("user", user);

                if (!bindingResult.hasErrors()) {
                    Long order = null;
                    try {
                        order = placeOrderService.saveOrder(placeOrderForm, user, shoppingCart.cartSumValue(), shoppingCart);
                    } catch (Exception e) {
                        return "redirect:/cart/all?error";
                    }

                    shoppingCart.clearCart();
                    model.addAttribute("orderID", order);
                    redirectAttributes.addFlashAttribute("successMessage", "Zamówienie nr #" + order + "Twoje zamówienie zostało przyjęte do realizacji.<br />Status zamówienia można sprawdzić w panelu klienta.");
                    return "redirect:/cart/all";

                }

            } else {
                return "redirect:/cart/all";
            }

        } else {
            redirectAttributes.addFlashAttribute("errorMessage", "Musisz być zalogowany aby złożyć zamówienie.");
            return "redirect:/login";
        }

        return "home/index";

    }

}
