package pl.stopzone.shop.controller.home;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.stopzone.shop.component.ShoppingCart;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/default")
public class DefaultController {

    private final ShoppingCart shoppingCart;

    public DefaultController(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    @RequestMapping
    public String redirect(HttpServletRequest request) {

        if (shoppingCart.getProducts().size() > 0) {
            return "redirect:/cart/all";
        }

        if (request.isUserInRole("ROLE_ADMIN")) {
            return "redirect:/admin";
        }

        if (request.isUserInRole("ROLE_USER")) {
            return "redirect:/panel";
        }

        return "redirect:/login";

    }

}
