package pl.stopzone.shop.controller.home;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.logging.Level;
import java.util.logging.Logger;

@ControllerAdvice
public class ErrorController {

    private String text = "Strona, której szukasz nie została znaleziona. Zostałeś przekierowany na główną stronę serwisu.";

    @ExceptionHandler(Exception.class)
    public String handleError(HttpServletRequest request, Exception e, RedirectAttributes redirectAttributes) {

        Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Request: " + request.getRequestURL() + " raised " + e);
        redirectAttributes.addFlashAttribute("errorMessage", text);
        return "redirect:/";

    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public String handleError404(HttpServletRequest request, Exception e, RedirectAttributes redirectAttributes) {

        Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Request: " + request.getRequestURL() + " raised " + e);
        redirectAttributes.addFlashAttribute("errorMessage", text);
        return "redirect:/";

    }

}