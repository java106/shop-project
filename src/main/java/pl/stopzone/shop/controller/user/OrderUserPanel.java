package pl.stopzone.shop.controller.user;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.stopzone.shop.controller.user.core.UserBreadcrumbs;
import pl.stopzone.shop.controller.user.core.UserCoreController;
import pl.stopzone.shop.dto.OrderDto;
import pl.stopzone.shop.service.order.OrderService;
import pl.stopzone.shop.service.user.UserService;

import java.util.List;

@Controller
@RequestMapping("/panel/orders")
public class OrderUserPanel extends UserCoreController {

    private OrderService orderService;
    private UserService userService;

    public OrderUserPanel(OrderService orderService, UserService userService) {
        this.orderService = orderService;
        this.userService = userService;
    }

    @RequestMapping
    public String index(Model model) {

        UserBreadcrumbs br = new UserBreadcrumbs();
        br.add("Moje zamówienia", "");

        List<OrderDto> allOrdersByUser = orderService.getAllOrdersByUser(userService.findByEmail(getUserName()));

        System.out.println(allOrdersByUser);

        model.addAttribute("loggedUsername", getUserName());
        model.addAttribute("content", "ordersUserPanel");
        model.addAttribute("title", "Panel Użytkownika");
        model.addAttribute("breadcrumbs", br.get());
        model.addAttribute("menu", userMenu);
        model.addAttribute("orders", allOrdersByUser);

        return "user/index";
    }

}
