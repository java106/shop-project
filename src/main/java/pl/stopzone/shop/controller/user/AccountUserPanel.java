package pl.stopzone.shop.controller.user;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.stopzone.shop.controller.user.core.UserBreadcrumbs;
import pl.stopzone.shop.controller.user.core.UserCoreController;
import pl.stopzone.shop.dto.AddressDto;
import pl.stopzone.shop.dto.UserDto;
import pl.stopzone.shop.mapper.UserMapper;
import pl.stopzone.shop.service.address.AddressService;
import pl.stopzone.shop.service.user.UserService;
import pl.stopzone.shop.validator.AddAddressFormValidation;
import pl.stopzone.shop.validator.UpdateUserValidator;

import java.util.List;

@Controller
public class AccountUserPanel extends UserCoreController {

    @InitBinder("userData")
    void iniBinderUpdateUser(WebDataBinder binder) { binder.setValidator(new UpdateUserValidator()); }

    @InitBinder("addressDto")
    void iniBinderUser(WebDataBinder binder) { binder.setValidator(new AddAddressFormValidation()); }

    private AddressService addressService;
    private UserService userService;

    public AccountUserPanel(AddressService addressService, UserService userService) {
        this.addressService = addressService;
        this.userService = userService;
    }

    @GetMapping("/panel/my-account")
    public String index(Model model) {

        UserBreadcrumbs br = new UserBreadcrumbs();
        br.add("Moje konto", "");

        UserDto userDto = UserMapper.mapToDto(userService.findByEmail(getUserName()));
        List<AddressDto> addressess = addressService.findByUserEmail(getUserName());

        model.addAttribute("loggedUsername", getUserName());
        model.addAttribute("content", "accountUserPanel");
        model.addAttribute("title", "Panel Użytkownika");
        model.addAttribute("breadcrumbs", br.get());
        model.addAttribute("menu", userMenu);
        model.addAttribute("userData", userDto);
        model.addAttribute("addressDto", new AddressDto());
        model.addAttribute("addresses", addressess);

        return "user/index";
    }

    @PostMapping("/panel/my-account/edit-user-data")
    public String updateUserDataPanel(@ModelAttribute("userData") @Validated UserDto userData, BindingResult bindingResult,
                                      RedirectAttributes redirectAttributes, Model model) {

        UserDto user = UserMapper.mapToDto(userService.findByEmail(getUserName()));

        if (!bindingResult.hasErrors()) {
            userData.setId(user.getId());
            userService.updateUser(userData);
            redirectAttributes.addFlashAttribute("successMessage", "Poprawnie zaktualizowano dane.");
            return "redirect:/panel/my-account";
        }

        redirectAttributes.addFlashAttribute("errorMessage", "Podane dane są nieporawidłowe.");
        return "redirect:/panel/my-account";
    }

    @PostMapping("/panel/my-account/edit-add-address")
    public String addAddresUserPanel(@ModelAttribute("addressDto") @Validated AddressDto addressDto, BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        UserDto user = UserMapper.mapToDto(userService.findByEmail(getUserName()));

        if (!bindingResult.hasErrors()) {
            addressService.saveAddress(addressDto, Math.toIntExact(user.getId()));
            redirectAttributes.addFlashAttribute("successMessage", "Dodano nowy adres.");
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", "Formularz zawiera błędy.");
        }

        return "redirect:/panel/my-account";

    }

}
