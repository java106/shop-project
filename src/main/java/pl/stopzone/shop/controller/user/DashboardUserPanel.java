package pl.stopzone.shop.controller.user;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.stopzone.shop.controller.user.core.UserBreadcrumbs;
import pl.stopzone.shop.controller.user.core.UserCoreController;
import pl.stopzone.shop.dto.UserDto;
import pl.stopzone.shop.service.address.AddressService;
import pl.stopzone.shop.service.user.UserService;

@Controller
@RequestMapping("/panel")
public class DashboardUserPanel extends UserCoreController {

    private UserService userService;
    private AddressService addressService;

    public DashboardUserPanel(UserService userService, AddressService addressService) {
        this.userService = userService;
        this.addressService = addressService;
    }

    @RequestMapping
    public String index(Model model) {

        UserBreadcrumbs br = new UserBreadcrumbs();
        br.add("Dashboard", "");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();

        UserDto user = userService.findDataByUserEmail(userName);
        user.setAddressList(addressService.findByUserEmail(userName));

        model.addAttribute("loggedUsername", getUserName());
        model.addAttribute("content", "startUserPanel");
        model.addAttribute("title", "Panel Użytkownika");
        model.addAttribute("breadcrumbs", br.get());
        model.addAttribute("menu", userMenu);
        model.addAttribute("userData", user);


        return "user/index";
    }

}