package pl.stopzone.shop.controller.user.core;

import lombok.NoArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.LinkedHashMap;
import java.util.Map;

@NoArgsConstructor
public class UserCoreController {

    protected Map<String, String> userMenu = new LinkedHashMap<>() {{
        put("Dashboard", "/panel");
        put("Moje zamówienia", "/panel/orders");
        put("Moje konto", "/panel/my-account");
        put("Wyloguj się", "/logout");
    }};

    protected String getUserName() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName();
    }

}
