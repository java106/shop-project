package pl.stopzone.shop.controller.user.core;

import java.util.LinkedHashMap;
import java.util.Map;

public class UserBreadcrumbs {

    private Map<String, String> breadcrumbs = new LinkedHashMap<>();

    public UserBreadcrumbs() {
        this.breadcrumbs.put("Panel użytkownika", "/panel");
    }

    public void add(String s1, String s2) {
        breadcrumbs.put(s1, s2);
    }

    public Map<String, String> get() {
        return breadcrumbs;
    }

}