package pl.stopzone.shop.controller.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.stopzone.shop.dto.RegisterUserForm;
import pl.stopzone.shop.model.User;
import pl.stopzone.shop.service.user.UserService;

@Controller
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    private UserService userService;

    private final Validator validator;

    public RegisterController(@Qualifier("createUserFormValidator") Validator validator) {
        this.validator = validator;
    }

    @InitBinder
    void iniBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @GetMapping
    public ModelAndView registration(){
        ModelAndView modelAndView = new ModelAndView("register");
        RegisterUserForm registerUserForm = new RegisterUserForm();
        modelAndView.addObject("registerUserForm", registerUserForm);
        return modelAndView;
    }

    @PostMapping
    public ModelAndView createNewUser(@ModelAttribute @Validated RegisterUserForm registerUserForm, BindingResult bindingResult) {

        ModelAndView modelAndView = new ModelAndView();

        if (!bindingResult.hasErrors()) {
            User userExists = userService.findByEmail(registerUserForm.getEmail());

            if (userExists != null) {
                bindingResult.rejectValue("email", "error.user","Podany adres e-mail istnieje w systemie.");
            } else {
                userService.saveUser(registerUserForm);
                modelAndView.addObject("successMessage", "Użytkownik został zarejestrowany.");
                modelAndView.setViewName("login");
            }

        }
        return modelAndView;
    }

}
