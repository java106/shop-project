package pl.stopzone.shop.component;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pl.stopzone.shop.dto.ProductDto;
import pl.stopzone.shop.service.product.ProductService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ShoppingCart {

    private ProductService productService;
    private List<ProductDto> products = new ArrayList<>();

    public ShoppingCart(ProductService productService) {
        this.productService = productService;
    }

    public void addProductToCart(Long idProduct) {
        products.add(productService.getProductById(idProduct));
    }

    public void removeFromCart(long idProduct) {
        products.removeIf(productDTO -> productDTO.getProductId() == idProduct);
    }

    public List<ProductDto> getProducts() {
        return products;
    }

    public BigDecimal cartSumValue() {
        BigDecimal out = new BigDecimal(0);
        for (ProductDto product : products) {
            out = out.add(new BigDecimal(product.getProductPrice()));
        }
        return out;
    }

    public Integer countCartProducts() {
        return products.size();
    }

    public void clearCart() {
        products.clear();
    }

}

